package hu.usstom.spring.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.usstom.spring.entity.Calculate;
import hu.usstom.spring.entity.TestEntity;
import hu.usstom.spring.entity.Transaction;
import hu.usstom.spring.entity.User;
import hu.usstom.spring.service.TestService;
import hu.usstom.spring.service.TransactionService;
import hu.usstom.spring.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private TransactionService transactionService;
	@Autowired
	private TestService testService;

	@RequestMapping("/saveTestEntity")
	public String saveTestEntit() {

		TestEntity theTest = new TestEntity();
		/*
		List<User> theUsers = userService.getUsers();
		theTest.setUser( theUsers.get(2));
		theTest.setAmount(6666);
		theTest.setTime(new Date(System.currentTimeMillis()));
		theTest.setAddtime(new Date(System.currentTimeMillis()));
		theTest.setIsmonthly(true);
		// save the customer using our service
	 */
		
		//testService.saveTestEntity(theTest);
		
		Transaction t = new Transaction();
		
		List<User> theUsers = userService.getUsers();
		t.setUser(theUsers.get(2));
		t.setAddtime(new Date(System.currentTimeMillis()));
		t.setTime(new Date(System.currentTimeMillis()));
		t.setAmount(1337);
		t.setDescript("leiras");
		t.setIsmonthly(true);
		t.setTitle("cim");
		
		transactionService.saveTransaction(t);

		return "redirect:/user/list";
	}
	
	@GetMapping("/list")
	public String listCostumer(Model theModel) {

		List<User> theUsers = userService.getUsers();

		theModel.addAttribute("users", theUsers);

		return "list-users";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {

		// create model attribute to bind form data
		User theUser = new User();

		theModel.addAttribute("user", theUser);

		return "user-form";
	}

	@PostMapping("/saveUser")
	public String saveCustomer(@ModelAttribute("user") User theUser) {

		// save the customer using our service
		userService.saveUser(theUser);

		return "redirect:/user/list";
	}

	@GetMapping("/showFormForAddTrans")
	public String showFormForAddTrans(@RequestParam("userId") int theId, Model theModel) {

		// create model attribute to bind form data
		Transaction theTransaction = new Transaction();
		User theUser = userService.getUser(theId);
		/*
		Transaction t = new Transaction();

		t.setAddtime(new Date(System.currentTimeMillis()));
		t.setTime(new Date(System.currentTimeMillis()));
		t.setAmount(1337);
		t.setDescript("leiras");
		t.setIsmonthly(true);
		t.setTitle("cim");
		
		// get the customer from our service
		
		
		t.setUser(theUser);
		
		transactionService.saveTransaction(t);
		*/
		theTransaction.setUser(theUser);
		// set customer as a model attribute to pre-populate the form
		theModel.addAttribute("transaction", theTransaction);
		theModel.addAttribute("user", theUser);

		return "transaction-form";
	}

	@PostMapping("/saveTransaction")
	public String saveTransaction(@ModelAttribute("transaction") Transaction theTransaction) {

		int param = theTransaction.getUser().getId();
		// save the customer using our service
		transactionService.saveTransaction(theTransaction);

		return "redirect:/user//profile?userId="+param;
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("userId") int theId, Model theModel) {

		// get the customer from our service
		User theUser = userService.getUser(theId);

		// set customer as a model attribute to pre-populate the form
		theModel.addAttribute("user", theUser);

		// send over to our form
		return "user-form";
	}

	@GetMapping("/delete")
	public String deleteCustomer(@RequestParam("userId") int theId) {
		// first delete all transaction by user
		List<Transaction> deleteTranslist = transactionService.getTransactionsByUserID(theId);
		for (Transaction transaction : deleteTranslist) {
			transactionService.deleteTransaction(transaction.getId());
		}
		// delete the customer
		userService.deleteUser(theId);

		return "redirect:/user/list";
	}
	
	@GetMapping("/deletetrans")
	public String deleteTransaction(@RequestParam("transactionId") int theId) {

		int param = transactionService.getTransaction(theId).getUser().getId();
		
		
		// delete the customer
		transactionService.deleteTransaction(theId);

		return "redirect:/user//profile?userId="+param;
	}

	@GetMapping("/profile")
	public String showprofile(@RequestParam("userId") int theId, Model theModel) {

		// get the customer from our service
		User theUser = userService.getUser(theId);
		List<Transaction> theTransactions = transactionService.getTransactionsByUserID(theId);
		Calculate calc = new Calculate(theTransactions);
		System.out.println(calc.getSumm());
		
		// set customer as a model attribute to pre-populate the form
		theModel.addAttribute("user", theUser);
		theModel.addAttribute("transactions", theTransactions);
		theModel.addAttribute("calc", calc);

		// System.out.println(theTransaction.toString());

		// send over to our form
		return "user-profile-form";
	}
}
