package hu.usstom.spring.service;

import java.util.List;

import hu.usstom.spring.entity.Transaction;

public interface TransactionService {

	public List<Transaction> getTransactions();

	public List<Transaction> getTransactionsByUserID(int theUserId);

	public void saveTransaction(Transaction theTransaction);

	public Transaction getTransaction(int theId);

	public void deleteTransaction(int theId);

}
