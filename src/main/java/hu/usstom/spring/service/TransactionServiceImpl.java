package hu.usstom.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.usstom.spring.dao.TransactionDAO;
import hu.usstom.spring.entity.Transaction;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionDAO transactionDAO;
	
	
	@Override
	@Transactional
	public List<Transaction> getTransactions() {
		
		return transactionDAO.getTransactions();
		
	}

	@Override
	@Transactional
	public List<Transaction> getTransactionsByUserID(int theUserId) {
		
		return transactionDAO.getTransactionsByUserID(theUserId);
	}

	@Override
	@Transactional
	public void saveTransaction(Transaction theTransaction) {
		
		transactionDAO.saveTransaction(theTransaction);
		
	}

	@Override
	@Transactional
	public Transaction getTransaction(int theId) {

		return transactionDAO.getTransaction(theId);
		
	}

	@Override
	@Transactional
	public void deleteTransaction(int theId) {

		transactionDAO.deleteTransaction(theId);
		
	}

}
