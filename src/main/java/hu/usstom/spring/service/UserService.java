package hu.usstom.spring.service;

import java.util.List;

import hu.usstom.spring.entity.User;

public interface UserService {

	public List<User> getUsers();

	public void saveUser(User theUser);

	public User getUser(int theId);

	public void deleteUser(int theId);
	
}
