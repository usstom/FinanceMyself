package hu.usstom.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hu.usstom.spring.dao.TestDAO;
import hu.usstom.spring.entity.TestEntity;

@Service
public class TestServiceImpl implements TestService{

	@Autowired
	private TestDAO testDAO;
	
	@Override
	@Transactional
	public void saveTestEntity(TestEntity theTest) {
		
		testDAO.saveTestEntity(theTest);
		
	}


}
