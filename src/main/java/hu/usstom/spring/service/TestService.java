package hu.usstom.spring.service;

import hu.usstom.spring.entity.TestEntity;

public interface TestService {

	public void saveTestEntity(TestEntity theTest);
	
}
