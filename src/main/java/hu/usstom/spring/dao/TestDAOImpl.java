package hu.usstom.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hu.usstom.spring.entity.TestEntity;

@Repository
public class TestDAOImpl implements TestDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void saveTestEntity(TestEntity theTest) {
		// get current hibernate session
				Session currentSession = sessionFactory.getCurrentSession();
					
				System.out.println(theTest);
				// save/upate the customer ... finally LOL
				currentSession.saveOrUpdate(theTest);
		
	}

}
