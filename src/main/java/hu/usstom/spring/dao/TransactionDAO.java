package hu.usstom.spring.dao;

import java.util.List;

import hu.usstom.spring.entity.Transaction;

public interface TransactionDAO {
	
	public List<Transaction> getTransactions();
	
	public List<Transaction> getTransactionsByUserID(int theUserId);

	public void saveTransaction(Transaction theTransaction);

	public Transaction getTransaction(int theId);

	public void deleteTransaction(int theId);

}
