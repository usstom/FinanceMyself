package hu.usstom.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hu.usstom.spring.entity.Transaction;

@Repository
public class TransactionDAOImpl implements TransactionDAO {

	// need to inject the session factory
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Transaction> getTransactions() {

		System.out.println("Trans dao impl flag");
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query ...
		Query<Transaction> theQuery = currentSession.createQuery("from Transaction", Transaction.class);

		// execute query and get result list
		List<Transaction> transactions = theQuery.getResultList();

		/*
		 * Transaction trans = new Transaction(); trans.setAmount(50000);
		 * 
		 * transactions.add(trans);
		 */

		System.out.println(transactions.toString());

		// return the results
		return transactions;
	}

	@Override
	public List<Transaction> getTransactionsByUserID(int theUserId) {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// create a query ...
		Query<Transaction> theQuery = currentSession.createQuery("from Transaction where userid=:userId",
				Transaction.class);
		theQuery.setParameter("userId", theUserId);
		// execute query and get result list
		List<Transaction> transactions = theQuery.getResultList();

		System.out.println(transactions.toString());

		// return the results
		return transactions;
	}

	@Override
	public void saveTransaction(Transaction theTransaction) {
		// get current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// save/upate the customer ... finally LOL
		System.out.println("DAO: "+theTransaction.toString());
		currentSession.saveOrUpdate(theTransaction);

	}

	@Override
	public Transaction getTransaction(int theId) {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// now retrieve/read from database using the primary key
		Transaction theTransaction = currentSession.get(Transaction.class, theId);

		return theTransaction;
	}

	@Override
	public void deleteTransaction(int theId) {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();

		// delete object with primary key
		Query theQuery = currentSession.createQuery("delete from Transaction where id=:TransactionId");
		theQuery.setParameter("TransactionId", theId);

		theQuery.executeUpdate();

	}

}
