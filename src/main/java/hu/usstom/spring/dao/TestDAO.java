package hu.usstom.spring.dao;

import hu.usstom.spring.entity.TestEntity;

public interface TestDAO {

	public void saveTestEntity(TestEntity theTest);
}
