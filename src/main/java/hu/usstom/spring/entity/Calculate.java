package hu.usstom.spring.entity;

import java.util.List;

public class Calculate {

	private List<Transaction> list;
	private double summ;
	
	
	public Calculate(List<Transaction> list) {
		super();
		this.list = list;
		this.summ=summAllTransaction(list);
	}
	
	private double summAllTransaction(List<Transaction> list) {
		double summ = 0;
		for (Transaction transaction : list) {
			summ = summ + transaction.getAmount();
		}
		return summ;
	}

	public double getSumm() {
		return summ;
	}
	
	
}
