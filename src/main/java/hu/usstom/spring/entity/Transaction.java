package hu.usstom.spring.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@OneToOne
	@JoinColumn(name = "userid")
	private User user;

	@Column(name = "amount")
	private double amount;

	@Column(name = "ismonthly")
	private boolean ismonthly;

	@Column(name = "time")
	private Date time;

	@Column(name = "addtime")
	private Date addtime;

	@Column(name = "title")
	private String title;

	@Column(name = "descript")
	private String descript;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isIsmonthly() {
		return ismonthly;
	}

	public void setIsmonthly(boolean ismonthly) {
		this.ismonthly = ismonthly;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Date getAddtime() {
		return addtime;
	}

	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", user=" + user + ", amount=" + amount + ", ismonthly=" + ismonthly
				+ ", time=" + time + ", addtime=" + addtime + ", title=" + title + ", desc=" + descript + "]";
	}

	
	
}
