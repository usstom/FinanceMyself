<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>

<head>
<title>Save Transaction</title>

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/add-customer-style.css">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>FinanceMyself - New Transaction</h2>
		</div>
	</div>

	<div id="container">
		<h3>Save Transaction</h3>

		<form:form action="saveTransaction" modelAttribute="transaction"
			method="POST">

			<!-- need to associate this data with user id -->
			<form:hidden path="id" />
			<form:hidden path="user.id" value="${transaction.user.id}" />
			<form:hidden path="user.name" value="${transaction.user.name}" />
			<form:hidden path="user.password"
				value="${transaction.user.password}" />
			<form:hidden path="user.email" value="${transaction.user.email}" />

			<table>
				<tbody>
					<tr>
						<td><label>User Name:</label></td>
						<td>${transaction.user.name}</td>
					</tr>

					<tr>
						<td><label>User Id:</label></td>
						<td>${transaction.user.id}</td>
					</tr>

					<tr>
						<td><label>Amount:</label></td>
						<td><form:input path="amount" /></td>
					</tr>

					<tr>
						<td><label>Time:</label></td>
						<td><form:input path="time" value="yyyy/mm/dd"
								id="datepicker" /></td>
					</tr>

					<tr>
						<td><label>Title:</label></td>
						<td><form:input path="title" /></td>
					</tr>

					<tr>
						<td><label>Description:</label></td>
						<td><form:input path="descript" /></td>
					</tr>

					<tr>
						<td><label>Monthly:</label></td>
						<td><form:checkbox path="ismonthly" /></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>


				</tbody>
			</table>


		</form:form>

		<div style=""></div>

		<p>
			<a href="${pageContext.request.contextPath}/user/list">Back to
				ProfileList</a>
		</p>

	</div>

</body>

</html>










