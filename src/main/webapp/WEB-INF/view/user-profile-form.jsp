<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

<head>
<title>User Profile</title>

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/add-customer-style.css">
</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>User - Profile</h2>
		</div>
	</div>

	<div id="container">
		<h3>User - Profile</h3>

		<form:form action="saveUser" modelAttribute="user" method="POST">

			<!-- need to associate this data with user id -->
			<form:hidden path="id" />

			<table>
				<tbody>
					<tr>
						<td><label>Name:</label></td>
						<td><form:input path="name" /></td>
					</tr>

					<tr>
						<td><label>Password:</label></td>
						<td><form:input path="password" /></td>
					</tr>

					<tr>
						<td><label>Email:</label></td>
						<td><form:input path="email" /></td>
					</tr>
					
					<tr>
						<td><label>Summ:</label></td>
						<td>${calc.summ}</td>
					</tr>
					<!-- we wont a save button
					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>
 -->

				</tbody>
			</table>


		</form:form>

		<div style=""></div>

		<p>
			<a href="${pageContext.request.contextPath}/user/list">Back to
				List</a>
		</p>

		<c:url var="addTrans" value="/user/showFormForAddTrans">
			<c:param name="userId" value="${user.id}" />
		</c:url>
		<p>
			<a href="${addTrans}">Add Trans</a>
		</p>
	</div>


	<div id="container">

		<div id="content">

			<!--  add our html table here -->

			<table>
				<tr>
					<th>User Name</th>
					<th>Amount</th>
					<th>Title</th>
					<th>Description</th>
					<th>Monthly</th>
					<th>Time</th>
					<th>Add Time</th>
					<th>Action</th>
				</tr>

				<!-- loop over and print our customers -->
				<c:forEach var="tempTransaction" items="${transactions}">

					<!-- construct an "profile" link with user id -->
					<c:url var="addTrans" value="/user/showFormForAddTrans">
						<c:param name="userId" value="${user.id}" />
					</c:url>
					
					<!-- construct an "delete" link with user id -->
					<c:url var="deleteLink" value="/user/deletetrans">
						<c:param name="transactionId" value="${tempTransaction.id}" />
					</c:url>

					<tr>
						<td>${tempTransaction.user.name}</td>
						<td>${tempTransaction.amount}</td>
						<td>${tempTransaction.title}</td>
						<td>${tempTransaction.descript}</td>
						<td>${tempTransaction.ismonthly}</td>
						<td>${tempTransaction.time}</td>
						<td>${tempTransaction.addtime}</td>

						<td>Update | <a href="${deleteLink}"
							   onclick="if (!(confirm('Are you sure you want to delete this transaction?'))) return false">Delete</a>
						</td>

					</tr>

				</c:forEach>

			</table>

		</div>

	</div>

</body>

</html>










