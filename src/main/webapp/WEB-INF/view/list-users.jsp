<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
	<title>List Users</title>
	
	<!-- reference our style sheet -->

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>Finance - User Manager</h2>
		</div>
	</div>
	
	<div id="container">
	
		<div id="content">
		
			<!-- put new button: Add Customer -->
		
			<input type="button" value="Add User"
				   onclick="window.location.href='showFormForAdd'; return false;"
				   class="add-button"
			/>
		<!--
			<c:url var="demo" value="/user/saveTestEntity">
			</c:url>
		
		<p>
			<a href="${demo}">Demo save</a>
		</p>
		-->
			<!--  add our html table here -->
		
			<table>
				<tr>
					<th>Name</th>
					<th>Password</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
				
				<!-- loop over and print our users -->
				<c:forEach var="tempUser" items="${users}">
				
					<!-- construct an "update" link with customer id -->
					<c:url var="updateLink" value="/user/showFormForUpdate">
						<c:param name="userId" value="${tempUser.id}" />
					</c:url>					

					<!-- construct an "delete" link with user id -->
					<c:url var="deleteLink" value="/user/delete">
						<c:param name="userId" value="${tempUser.id}" />
					</c:url>	
					
					<!-- construct an "profile" link with user id -->
					<c:url var="profileLink" value="/user/profile">
						<c:param name="userId" value="${tempUser.id}" />
					</c:url>				
					
					<tr>
						<td> ${tempUser.name} </td>
						<td> ${tempUser.password} </td>
						<td> ${tempUser.email} </td>
						
						<td>
							<!-- display the update link -->
							<a href="${updateLink}">Update</a>
							|
							<a href="${deleteLink}"
							   onclick="if (!(confirm('Are you sure you want to delete this user and they all transaction?'))) return false">Delete</a>
						    |
							<a href="${profileLink}">Profile</a>  
						</td>
						
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	

</body>

</html>









